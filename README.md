datalife_labor_yield_timesheet
==============================

The labor_yield_timesheet module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-labor_yield_timesheet/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-labor_yield_timesheet)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
