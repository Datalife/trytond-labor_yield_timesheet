# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import ModelSQL
from trytond.model import ModelView
from trytond.model import Unique
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.cache import Cache
from trytond.pyson import Eval, Or, Not, Bool
from trytond.transaction import Transaction
from trytond.config import config

__all__ = ['Work', 'WorkUnitPrice']

price_digits = (16, config.getint(
        'yield_work_cost', 'price_decimal', default=4))


class Work(metaclass=PoolMeta):
    __name__ = 'timesheet.work'

    unit_price = fields.Function(fields.Numeric('Unit Price',
        digits=price_digits,
        help="Unit price for this yield work."), 'get_unit_price')
    unit_prices = fields.One2Many('timesheet.work_unit_price', 'work',
        'Unit Prices',
        states={'invisible': ~Eval('yield_available')},
        depends=['yield_available'],
        help="List of unit price over time")
    _unit_prices_cache = Cache('timesheet_work.unit_prices')

    @classmethod
    def __setup__(cls):
        super(Work, cls).__setup__()
        ro_st = cls.uom_category.states.get('readonly', None)
        new_ro_st = Not(Bool(Eval('unit_prices')))
        ro_st = Or(ro_st, new_ro_st) if ro_st else new_ro_st
        cls.uom_category.states['readonly'] = ro_st
        cls.uom_category.depends.append('unit_prices')

    def get_unit_price(self, name):
        ctx_date = Transaction().context.get('date', None)
        return self.compute_unit_price(ctx_date)[0]

    def get_work_prices(self):
        pool = Pool()
        UnitPrice = pool.get('timesheet.work_unit_price')

        work_prices = self._unit_prices_cache.get(self.id)
        if work_prices is None:
            _prices = UnitPrice.search([
                ('work', '=', self.id),
            ], order=[('date', 'ASC')])

            work_prices = []
            for _price in _prices:
                work_prices.append(
                    (_price.date, _price.unit_price, _price.unit.id))
            self._unit_prices_cache.set(self.id, work_prices)
        return work_prices

    def compute_unit_price(self, date=None, to_uom=None):
        pool = Pool()
        Date = pool.get('ir.date')
        Uom = pool.get('product.uom')

        work_prices = self.get_work_prices()

        if date is None:
            date = Date.today()
        _price = 0
        _uom = to_uom
        if work_prices and date >= work_prices[0][0]:
            for edate, ecost, unit in work_prices:
                if date >= edate:
                    if to_uom:
                        _price = Decimal(
                            Uom.compute_qty(
                                to_uom,
                                float(ecost), Uom(unit))
                            ).quantize(Decimal(str(10 ** -price_digits[1])))
                    else:
                        _price = ecost
                        _uom = Uom(unit)
                else:
                    break
        return _price, _uom


class WorkUnitPrice(ModelSQL, ModelView):
    """Work Unit Price"""
    __name__ = 'timesheet.work_unit_price'

    date = fields.Date('Date', required=True, select=True)
    unit_price = fields.Numeric('Unit Price', required=True,
        digits=price_digits)
    unit = fields.Many2One('product.uom', 'UOM', required=True,
        domain=[
            ('category', '=',
                Eval('_parent_work', {}).get('uom_category', None))],
        depends=['work'])
    work = fields.Many2One('timesheet.work', 'Work', required=True)

    @classmethod
    def __setup__(cls):
        super(WorkUnitPrice, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('work_date_unit_price_uniq',
                Unique(t, t.work, t.date),
                'labor_yield_timesheet.'
                'msg_timesheet_work_unit_price_date_unique'),
            ]
        cls._order.insert(0, ('date', 'DESC'))

    @staticmethod
    def default_unit_price():
        return Decimal(0)

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')

        return Date.today()

    def get_rec_name(self, name):
        return str(self.date)

    @staticmethod
    def default_unit():
        pool = Pool()
        Modeldata = pool.get('ir.model.data')

        return Modeldata.get_id('product', 'uom_unit')

    @classmethod
    def delete(cls, prices):
        Work = Pool().get('timesheet.work')

        super(WorkUnitPrice, cls).delete(prices)
        Work._unit_prices_cache.clear()

    @classmethod
    def create(cls, vlist):
        Work = Pool().get('timesheet.work')

        prices = super(WorkUnitPrice, cls).create(vlist)
        Work._unit_prices_cache.clear()
        return prices

    @classmethod
    def write(cls, *args):
        Work = Pool().get('timesheet.work')

        super(WorkUnitPrice, cls).write(*args)
        Work._unit_prices_cache.clear()
