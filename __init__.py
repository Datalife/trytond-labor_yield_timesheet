# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .labor_yield import YieldAllocation
from .timesheet import (TimesheetLine, CreateLaborYieldTimesheet,
    CreateLaborYieldTimesheetStart, DeleteLaborYieldTimesheet,
    DeleteLaborYieldTimesheetStart)
from .work import Work, WorkUnitPrice


def register():
    Pool.register(
        Work,
        WorkUnitPrice,
        YieldAllocation,
        TimesheetLine,
        CreateLaborYieldTimesheetStart,
        DeleteLaborYieldTimesheetStart,
        module='labor_yield_timesheet', type_='model')
    Pool.register(
        CreateLaborYieldTimesheet,
        DeleteLaborYieldTimesheet,
        module='labor_yield_timesheet', type_='wizard')
